
# Formation informatique et programmation

## Module 1 Savoir utiliser son environnement

#### Un peu d'histoire sur l'informatique

- https://fr.wikipedia.org/wiki/Histoire_de_l%27informatique

### Chapitre 1 L'ordinateur

#### Composants d'un ordinateur

- Le processeur
- La mémoire vive (RAM)
- La carte graphique
- Le disque dur
- La carte mère.
- une alimentation.

#### C'est quoi le processeur de l'ordinateur ?

Le processeur ou CPU (Central Processing Unit) est le cerveau de l'ordinateur.
Il gère les échanges de données entre composants (mémoire RAM, disque dur, carte graphique).

#### C'est quoi la carte mère ?

La carte mère centralise la gestion de la RAM (ou mémoire à accès aléatoire),
les lectures de disque dur et l'utilisation du processeur.
Son rôle est également essentiel pour la détection et la compatibilité des cartes graphiques.
La gestion du réseau et des ports USB se fait également par son intermédiaire

#### C'est quoi la mémoire vive ?

La RAM de l'ordinateur, ou mémoire vive, est une sorte de réservoir où sont stockées des informations temporaires utilisées par le processeur.
Elle est donc essentielle au fonctionnement de l'ordinateur.
Le processeur la sollicite constamment pour exécuter un programme.

### Chapitre 2 Les réseaux

- https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet

### Chapitre 3 Savoir utiliser son système

- **Commandes linux de base**

| Commande |               Description               |
|----------|:---------------------------------------:|
| ls       |        Liste le dossier courant         |
| cd       |      ce deplacer dans les dossiers      |
| pwd      |          chemin dossier actuel          |
| mkdir    |            créer un dossier             |
| rm       |          supprimer un dossier           |
| cp       |        copier fichier ou dossier        |
| mv       |       deplacer fichier ou dossier       |
| ln       | lien symbolique vers fichier ou dossier |
| touch    |            créer un fichier             |
| cat      |             lire un fichier             |
| more     |        lire pas a pas un fichier        |
| head     |      afficher le debut du fichier       |
| tail     |       afficher la fin du fichier        |

**Comment faire quand on ne sait pas ?**

- Google
- la Documentation
- formations et tutos
- livres
- StackOverflow

### Chapitre 4 Les outils pour développer

**Un environnement de développement intégré, ou IDE**

- Est un logiciel de création d'applications,
  qui rassemble des outils de développement fréquemment utilisés dans une seule interface utilisateur graphique (GUI)

**Liste IDE:**

- https://code.visualstudio.com/
- https://www.jetbrains.com/fr-fr/
- https://www.sublimetext.com/
- https://atom.io/

**IDE En ligne**

- https://replit.com/

**Terminal**

- les logs des applications (debug, information, erreur)
- installation package, librairie, outils aide au développement.

**Navigateur internet**

- mode développeur
- extension ou plugins
